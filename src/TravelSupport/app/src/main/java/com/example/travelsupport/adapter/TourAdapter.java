package com.example.travelsupport.adapter;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.travelsupport.R;
import com.example.travelsupport.model.Tour;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TourAdapter extends ArrayAdapter<Tour> {
    Context context;
    int resource;
    private List<Tour> arrTour;

    public TourAdapter(@NonNull Context context, int resource, ArrayList<Tour> arrTour) {
        super(context, resource, arrTour);
        this.context = context;
        this.resource = resource;
        this.arrTour = arrTour;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.tour_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgAvatar = convertView.findViewById(R.id.imgAvatar);
            viewHolder.txtName = convertView.findViewById(R.id.txtName);
            viewHolder.txtDuration = convertView.findViewById(R.id.txtDuration);
            viewHolder.txtPeople = convertView.findViewById(R.id.txtPeople);
            viewHolder.txtPrice = convertView.findViewById(R.id.txtPrice);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Tour tour=arrTour.get(position);

        if(tour.getAvatar() == null) {
            viewHolder.imgAvatar.setImageResource(R.drawable.image);
        } else {
            Picasso.get().load(tour.getAvatar().toString()).into(viewHolder.imgAvatar);
        }
        viewHolder.txtName.setText(tour.getName());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String duration;
        try {
            duration = dateFormat.format(Long.parseLong(tour.getStartDate())) + " - " + dateFormat.format(Long.parseLong(tour.getEndDate()));
        } catch (Exception e) {
            e.printStackTrace();
            duration = "error date";
        }
        viewHolder.txtDuration.setText(duration);
        viewHolder.txtPeople.setText(tour.getAdults().toString());

        String price = tour.getMinCost() + " - " + tour.getMaxCost();
        viewHolder.txtPrice.setText(price + " VND");
        return convertView;
    }

    private class ViewHolder {
        TextView txtName, txtDuration, txtPeople, txtPrice;
        ImageView imgAvatar;
    }
}
