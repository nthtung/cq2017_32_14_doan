package com.example.travelsupport.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelsupport.R;
import com.example.travelsupport.adapter.TourAdapter;
import com.example.travelsupport.model.HistoryTourRequest;
import com.example.travelsupport.model.HistoryTourResponse;
import com.example.travelsupport.model.SearchHistoryResponse;
import com.example.travelsupport.model.Tour;
import com.example.travelsupport.model.TourListRequest;
import com.example.travelsupport.model.TourListResponse;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;
import com.example.travelsupport.view.TourDetailActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment {
    private UserService userService;
    private EditText edtSearchHistory;
    private Button btnSearchHistory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_history, container, false);
        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);
        edtSearchHistory=view.findViewById(R.id.edtSearchHistory);
        btnSearchHistory=view.findViewById(R.id.btnSearchHistory);

        btnSearchHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchKey = edtSearchHistory.getText().toString();

                Call<SearchHistoryResponse> call =userService.searchHistory(MyAPIClient.getInstance().getAccessToken(), searchKey, 1,10000);

                call.enqueue(new Callback<SearchHistoryResponse>() {
                    @Override
                    public void onResponse(Call<SearchHistoryResponse> call, Response<SearchHistoryResponse> response) {
                        if (response.isSuccessful()) {
                            List<Tour> listTour = response.body().getTours();
                            final ArrayList<Tour> arrTour = new ArrayList<Tour>();
                            for (Tour tour : listTour) {
                                if (tour.getHostId().compareTo(MyAPIClient.getInstance().getUserID()) == 0) {
                                    arrTour.add(tour);
                                }
                            }

                            ListView lvTourHistory = view.findViewById(R.id.lvTourHistory);

                            lvTourHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Tour t = arrTour.get(position);

                                    Log.d("abc", "Clicked");

                                    Intent intent = new Intent(getActivity(), TourDetailActivity.class);
                                    intent.putExtra("tourId", t.getId());
                                    startActivity(intent);
                                }
                            });

                            TourAdapter tourAdapter = new TourAdapter(getActivity(), R.layout.tour_item, arrTour);
                            lvTourHistory.setAdapter(tourAdapter);
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchHistoryResponse> call, Throwable t) {

                    }
                });
            }
        });

        Call<HistoryTourResponse> call = userService.historyTour(MyAPIClient.getInstance().getAccessToken(), 1, 1);
        call.enqueue(new Callback<HistoryTourResponse>() {
            @Override
            public void onResponse(Call<HistoryTourResponse> call, Response<HistoryTourResponse> response) {
                Call<HistoryTourResponse> realCall = userService.historyTour(MyAPIClient.getInstance().getAccessToken(), 1, Integer.parseInt(response.body().getTotal()));
                realCall.enqueue(new Callback<HistoryTourResponse>() {
                    @Override
                    public void onResponse(Call<HistoryTourResponse> call, Response<HistoryTourResponse> response) {
                        if (response.isSuccessful()) {
                            List<Tour> listTour = response.body().getTours();
                            final ArrayList<Tour> arrTour = new ArrayList<Tour>();
                            for (Tour tour : listTour) {
                                if (tour.getHostId().compareTo(MyAPIClient.getInstance().getUserID()) == 0) {
                                    arrTour.add(tour);
                                }
                            }


                            ListView lvTourHistory = view.findViewById(R.id.lvTourHistory);

                            lvTourHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Tour t = arrTour.get(position);

                                    Log.d("abc", "Clicked");

                                    Intent intent = new Intent(getActivity(), TourDetailActivity.class);
                                    intent.putExtra("tourId", t.getId());
                                    startActivity(intent);
                                }
                            });

                            TourAdapter tourAdapter = new TourAdapter(getActivity(), R.layout.tour_item, arrTour);
                            lvTourHistory.setAdapter(tourAdapter);
                        }
                    }


                    @Override
                    public void onFailure(Call<HistoryTourResponse> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onFailure(Call<HistoryTourResponse> call, Throwable t) {

            }
        });

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
