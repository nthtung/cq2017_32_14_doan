package com.example.travelsupport.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchDestinationResponse {
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("stopPoints")
    @Expose
    private List<StopPoint2> stopPoints = null;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<StopPoint2> getStopPoints() {
        return stopPoints;
    }

    public void setStopPoints(List<StopPoint2> stopPoints) {
        this.stopPoints = stopPoints;
    }
}
