package com.example.travelsupport.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelsupport.R;
import com.example.travelsupport.adapter.StopPointAdapter;
import com.example.travelsupport.model.SearchDestinationResponse;
import com.example.travelsupport.model.StopPoint2;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;
import com.example.travelsupport.view.ServiceDetailActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExploreFragment extends Fragment {
    private UserService userService;
    Button btnSearch;
    EditText edtSearch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_explore, container, false);
        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);
        edtSearch = view.findViewById(R.id.edtSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchKey = edtSearch.getText().toString();

                Call<SearchDestinationResponse> call = userService.searchDestination(MyAPIClient.getInstance().getAccessToken(),searchKey, 1, 1000);
                call.enqueue(new Callback<SearchDestinationResponse>() {
                    @Override
                    public void onResponse(Call<SearchDestinationResponse> call, Response<SearchDestinationResponse> response) {
                        if (response.isSuccessful()) {
                            List<StopPoint2> stopPointList = response.body().getStopPoints();
                            final ArrayList<StopPoint2> arrStopPoint = new ArrayList<>(stopPointList);
                            ListView lvStopPoint = view.findViewById(R.id.lvDestination);

                            lvStopPoint.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Integer serviceId = arrStopPoint.get(position).getId();
                                    Intent intent = new Intent(getActivity(), ServiceDetailActivity.class);
                                    intent.putExtra("serviceId", serviceId);

                                    startActivity(intent);
                                }
                            });

                            StopPointAdapter stopPointAdapter = new StopPointAdapter(getActivity(), R.layout.destination_item, arrStopPoint);
                            lvStopPoint.setAdapter(stopPointAdapter);
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchDestinationResponse> call, Throwable t) {
                        Log.d("SEARCH_DES", t.getMessage());
                    }
                });
            }
        });

        return view;
    }
}
