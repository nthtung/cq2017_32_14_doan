package com.example.travelsupport.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.travelsupport.R;
import com.example.travelsupport.model.Review;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ReviewAdapter extends ArrayAdapter<Review> {
    private Context context;
    private int resource;
    private List<Review> reviewList;

    public ReviewAdapter(@NonNull Context context, int resource, ArrayList<Review> reviewList) {
        super(context, resource, reviewList);
        this.context = context;
        this.resource = resource;
        this.reviewList = reviewList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.review_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtUserName = convertView.findViewById(R.id.txtUserName);
            viewHolder.txtUserReview = convertView.findViewById(R.id.txtUserReview);
            viewHolder.userRating = convertView.findViewById(R.id.userRating);
            viewHolder.imgUserAvatar = convertView.findViewById(R.id.imgUserAvatar);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Review review = reviewList.get(position);

        if(review.getAvatar() == null) {
            viewHolder.imgUserAvatar.setImageResource(R.drawable.ic_user);
        } else {
            Picasso.get().load(review.getAvatar().toString()).into(viewHolder.imgUserAvatar);
        }

        viewHolder.txtUserName.setText(review.getName());
        viewHolder.txtUserReview.setText(review.getReview());

        viewHolder.userRating.setRating(review.getPoint());

        return convertView;
    }

    private class ViewHolder {
        TextView txtUserName, txtUserReview;
        ImageView imgUserAvatar;
        RatingBar userRating;
    }
}
