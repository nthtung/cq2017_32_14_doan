package com.example.travelsupport.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import com.example.travelsupport.R;
import com.example.travelsupport.network.MyAPIClient;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // load access token dùng SharePreferences
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.package_name),MODE_PRIVATE);
        String accessToken = sharedPreferences.getString(getString(R.string.saved_access_token),null);
        String userID = sharedPreferences.getString(getString(R.string.user_ID),null);
        MyAPIClient.getInstance().setAccessToken(accessToken);
        MyAPIClient.getInstance().setUserID(userID);

        Intent intent;
        // nếu không có access token hoặc token hết hạn
        if (TextUtils.isEmpty(accessToken)) {
            intent = new Intent(this, LoginActivity.class);
        } else { // ngược lại thì hiển thị màn hình danh sách tour
            intent = new Intent(this, MainActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        return;
    }
}
