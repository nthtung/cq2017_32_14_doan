package com.example.travelsupport.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelsupport.R;
import com.example.travelsupport.adapter.CommentAdapter;
import com.example.travelsupport.adapter.ReviewAdapter;
import com.example.travelsupport.model.Comment;
import com.example.travelsupport.model.GetTourInfoResponse;
import com.example.travelsupport.model.GetTourReviewRequest;
import com.example.travelsupport.model.GetTourReviewResponse;
import com.example.travelsupport.model.Review;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneralFragment extends Fragment {
    private UserService userService;
    private TextView txtName,txtDuration,txtPrice,txtPeople;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_general,container,false);

        Integer tourId = getActivity().getIntent().getExtras().getInt("tourId");

        userService= MyAPIClient.getInstance().getRetrofit().create(UserService.class);

        txtName=view.findViewById(R.id.txtNameGF);
        txtDuration=view.findViewById(R.id.txtDurationGF);
        txtPrice=view.findViewById(R.id.txtPriceGF);
        txtPeople=view.findViewById(R.id.txtPeopleGF);

        Call<GetTourInfoResponse> call = userService.getTourInfo(MyAPIClient.getInstance().getAccessToken(),tourId);

        call.enqueue(new Callback<GetTourInfoResponse>() {
            @Override
            public void onResponse(Call<GetTourInfoResponse> call, Response<GetTourInfoResponse> response) {
                if (response.isSuccessful()) {
                    String tourName = response.body().getName();
                    String duration;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        duration = dateFormat.format(Long.parseLong(response.body().getStartDate())) + " - " + dateFormat.format(Long.parseLong(response.body().getEndDate()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        duration = "error date";
                    }
                    String price = response.body().getMinCost() + " - " + response.body().getMaxCost();
                    Integer people = response.body().getAdults();

                    txtName.setText(tourName);
                    txtDuration.setText(duration);
                    txtPrice.setText(price);
                    txtPeople.setText(people.toString());

                    List<Comment> commentList = response.body().getComments();
                    final ArrayList<Comment> commentArrayList = new ArrayList<Comment>(commentList);
                    ListView lvComment = view.findViewById(R.id.lvComment);

                    CommentAdapter commentAdapter = new CommentAdapter(getActivity(),R.layout.comment_item, commentArrayList);

                    lvComment.setAdapter(commentAdapter);
                }
            }

            @Override
            public void onFailure(Call<GetTourInfoResponse> call, Throwable t) {
                Log.d("GENERAL_FRAGMENT", t.getMessage());
            }
        });



        return view;
    }
}
