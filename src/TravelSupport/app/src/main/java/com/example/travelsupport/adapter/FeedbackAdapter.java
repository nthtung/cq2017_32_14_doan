package com.example.travelsupport.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.travelsupport.R;
import com.example.travelsupport.model.Feedback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FeedbackAdapter extends ArrayAdapter<Feedback> {
    private Context context;
    private int resource;
    private List<Feedback> feedbackList;

    public FeedbackAdapter(@NonNull Context context, int resource, ArrayList<Feedback> feedbackList) {
        super(context, resource, feedbackList);
        this.context = context;
        this.resource = resource;
        this.feedbackList = feedbackList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.feedback_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgAvatar = convertView.findViewById(R.id.imgUserAvatar);
            viewHolder.txtName = convertView.findViewById(R.id.txtUserName);
            viewHolder.txtFeedback = convertView.findViewById(R.id.txtUserFeedback);
            viewHolder.ratingBar = convertView.findViewById(R.id.ratingBar);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Feedback feedback = feedbackList.get(position);
        if (feedback.getAvatar() == null) {
            viewHolder.imgAvatar.setImageResource(R.drawable.ic_user);
        } else {
            Picasso.get().load(feedback.getAvatar()).into(viewHolder.imgAvatar);
        }

        String userName = feedback.getName();
        if (!TextUtils.isEmpty(userName)) {
            viewHolder.txtName.setText(userName);
        } else {
            viewHolder.txtName.setText("no username");
        }

        viewHolder.txtFeedback.setText(feedback.getFeedback());

        viewHolder.ratingBar.setRating(feedback.getPoint());
        return convertView;
    }

    private class ViewHolder {
        ImageView imgAvatar;
        TextView txtName, txtFeedback;
        RatingBar ratingBar;
    }
}
