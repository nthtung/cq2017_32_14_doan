package com.example.travelsupport.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelsupport.R;
import com.example.travelsupport.adapter.StopPointAdapter2;
import com.example.travelsupport.model.GetTourInfoResponse;
import com.example.travelsupport.model.StopPoint3;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;
import com.example.travelsupport.view.ServiceDetailActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StopPointFragment extends Fragment {
    private UserService userService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_stop_point, container, false);
        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);
        Integer tourId = getActivity().getIntent().getExtras().getInt("tourId");

        Call<GetTourInfoResponse> call = userService.getTourInfo(MyAPIClient.getInstance().getAccessToken(), tourId);

        call.enqueue(new Callback<GetTourInfoResponse>() {
            @Override
            public void onResponse(Call<GetTourInfoResponse> call, Response<GetTourInfoResponse> response) {
                if (response.isSuccessful()) {
                    List<StopPoint3> stopPoint3List = response.body().getStopPoints();
                    final ArrayList<StopPoint3> stopPoint3ArrayList = new ArrayList<>(stopPoint3List);

                    ListView lvStopPoint = view.findViewById(R.id.lvStopPoint);

                    lvStopPoint.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Integer serviceId = stopPoint3ArrayList.get(position).getId();
                            Intent intent = new Intent(getActivity(), ServiceDetailActivity.class);
                            intent.putExtra("serviceId", serviceId);

                            startActivity(intent);
                        }
                    });

                    StopPointAdapter2 stopPointAdapter = new StopPointAdapter2(getActivity(), R.layout.destination_item, stopPoint3ArrayList);

                    lvStopPoint.setAdapter(stopPointAdapter);
                }
            }

            @Override
            public void onFailure(Call<GetTourInfoResponse> call, Throwable t) {
                Log.d("STOPPOINT_F", t.getMessage());
            }
        });
        return view;
    }
}
