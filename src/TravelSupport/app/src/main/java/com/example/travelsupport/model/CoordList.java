package com.example.travelsupport.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CoordList {
    @SerializedName("coordinateSet")
    @Expose
    private List<CoordinateSet> coordinateSet = null;

    public List<CoordinateSet> getCoordinateSet() {
        return coordinateSet;
    }

    public void setCoordinateSet(List<CoordinateSet> coordinateSet) {
        this.coordinateSet = coordinateSet;
    }
}
