package com.example.travelsupport.model;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.travelsupport.R;
import com.google.android.gms.maps.model.Marker;

import java.util.List;


public class PopupStopPoint {
    private EditText edtName,edtAddress,edtArrive,edtLeave,edtMinCost,edtMaxCost;


    public void showPopupWindow(final View view, final Marker marker, final List<StopPoint> stopPointList) {
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_stop_point,null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView,width,height,focusable);

        view.findViewById(R.id.stopPointView).post(new Runnable() {
            @Override
            public void run() {
                popupWindow.showAtLocation(view, Gravity.BOTTOM,0,0);
            }
        });


        TextView edtStopPointInfo = popupView.findViewById(R.id.edtStopPointInfo),
                edtStopPointRating = popupView.findViewById(R.id.edtStopPointRating),
                edtStopPointDelete = popupView.findViewById(R.id.edtStopPointDelete);

        edtStopPointInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupStopPointInfo popupStopPointInfo = new PopupStopPointInfo();
                popupStopPointInfo.showPopupWindow(view, stopPointList, marker.getPosition());
            }
        });

        edtStopPointDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                marker.remove();
            }
        });

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // đóng cửa sổ popup
                popupWindow.dismiss();
                return true;
            }
        });
    }



}
