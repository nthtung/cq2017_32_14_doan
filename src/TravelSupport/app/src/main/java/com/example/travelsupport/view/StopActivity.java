package com.example.travelsupport.view;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.View;

import com.example.travelsupport.R;
import com.example.travelsupport.model.CoordList;
import com.example.travelsupport.model.CoordinateSet;
import com.example.travelsupport.model.GetSuggestDestinationRequest;
import com.example.travelsupport.model.GetSuggestDestinationResponse;
import com.example.travelsupport.model.PopupListStopPoint;
import com.example.travelsupport.model.PopupStopPoint;
import com.example.travelsupport.model.StopPoint;
import com.example.travelsupport.model.StopPoint4;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StopActivity extends FragmentActivity implements OnMapReadyCallback {

    private UserService userService;
    private GoogleMap mMap;
    FloatingActionButton btnListStopPoint;
    private List<StopPoint> stopPointList = new ArrayList<StopPoint>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        final Integer tourId = Integer.parseInt(getIntent().getStringExtra("TOUR_ID"));

        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);

        btnListStopPoint = findViewById(R.id.btnListStopPoint);
        btnListStopPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupListStopPoint popUpListSP = new PopupListStopPoint();
                popUpListSP.showPopupWindow(v, stopPointList, tourId);
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        List<CoordinateSet> coordinateSets = new ArrayList<>();

        coordinateSets.add(new CoordinateSet(22.874833,102.132472));
        coordinateSets.add(new CoordinateSet(22.78,107.95));
        coordinateSets.add(new CoordinateSet(8.77,103.33));
        coordinateSets.add(new CoordinateSet(8.83,110.126));

        final GetSuggestDestinationRequest request = new GetSuggestDestinationRequest();
        List<CoordList> coordLists = new ArrayList<>();
        CoordList temp = new CoordList();
        temp.setCoordinateSet(coordinateSets);
        coordLists.add(temp);

        request.setCoordList(coordLists);
        request.setHasOneCoordinate(false);

        Call<GetSuggestDestinationResponse> call = userService.getSuggestDestination(MyAPIClient.getInstance().getAccessToken(), request);

        call.enqueue(new Callback<GetSuggestDestinationResponse>() {
            @Override
            public void onResponse(Call<GetSuggestDestinationResponse> call, Response<GetSuggestDestinationResponse> response) {
                if (response.isSuccessful()) {
                    List<StopPoint4> stopPoint4List = response.body().getStopPoints();

                    for (StopPoint4 stopPoint:stopPoint4List) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(stopPoint.getLat()),Double.parseDouble(stopPoint.getLong()))));

                    }
                }
            }

            @Override
            public void onFailure(Call<GetSuggestDestinationResponse> call, Throwable t) {

            }
        });


        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                // add marker
                mMap.addMarker(new MarkerOptions()
                        .position(latLng));
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                View v = findViewById(R.id.stopPointView);
                PopupStopPoint popupStopPoint = new PopupStopPoint();
                popupStopPoint.showPopupWindow(v, marker, stopPointList);
                return false;
            }
        });

    }
}
