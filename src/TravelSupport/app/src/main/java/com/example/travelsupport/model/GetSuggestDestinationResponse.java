package com.example.travelsupport.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSuggestDestinationResponse {
    @SerializedName("stopPoints")
    @Expose
    private List<StopPoint4> stopPoints = null;

    public List<StopPoint4> getStopPoints() {
        return stopPoints;
    }

    public void setStopPoints(List<StopPoint4> stopPoints) {
        this.stopPoints = stopPoints;
    }

}
