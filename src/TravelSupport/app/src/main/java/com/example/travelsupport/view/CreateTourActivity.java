package com.example.travelsupport.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.travelsupport.R;
import com.example.travelsupport.model.CreateTourRequest;
import com.example.travelsupport.model.CreateTourResponse;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;
import com.google.android.gms.maps.model.LatLng;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTourActivity extends AppCompatActivity {
    Button btnCreateTour;
    EditText edtTourName, edtStartDate, edtEndDate, edtAdults, edtChildren, edtMinCost, edtMaxCost;
    CheckBox cbIsPrivate;
    Button btnStartPoint, btnEndPoint;
    private UserService userService;
    LatLng latLngStart, latLngEnd;
    static final int PICK_MAP_START_POINT_REQUEST = 998;  // The request code
    static final int PICK_MAP_END_POINT_REQUEST = 999;  // The request code
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tour);
        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);

        AnhXa();

        btnStartPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPointIntent = new Intent(v.getContext(), MapsActivity.class);
                startActivityForResult(pickPointIntent, PICK_MAP_START_POINT_REQUEST);
            }
        });

        btnEndPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPointIntent = new Intent(v.getContext(), MapsActivity.class);
                startActivityForResult(pickPointIntent, PICK_MAP_END_POINT_REQUEST);
            }
        });

        btnCreateTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTour();
            }
        });
    }

    private void createTour() {
        boolean cancle = false;
        // clear error
        edtTourName.setError(null);
        edtStartDate.setError(null);
        edtEndDate.setError(null);
        edtAdults.setError(null);
        edtChildren.setError(null);
        edtMinCost.setError(null);
        edtMaxCost.setError(null);

        // lấy dữ liệu
        String tourName = edtTourName.getText().toString(),
                startDate = edtStartDate.getText().toString(),
                endDate = edtEndDate.getText().toString(),
                adults = edtAdults.getText().toString(),
                children = edtChildren.getText().toString(),
                minCost = edtMinCost.getText().toString(),
                maxCost = edtMaxCost.getText().toString();

        boolean isPrivate = cbIsPrivate.isChecked();


        // Kiểm tra hợp lệ
        if (TextUtils.isEmpty(tourName)) {
            edtTourName.setError(getString(R.string.empty_field));
            cancle = true;
        }
        if (TextUtils.isEmpty(startDate)) {
            edtStartDate.setError(getString(R.string.empty_field));
            cancle = true;
        }
        if (TextUtils.isEmpty(endDate)) {
            edtEndDate.setError(getString(R.string.empty_field));
            cancle = true;
        }
        if (TextUtils.isEmpty(minCost)) {
            minCost = "0";
        }
        if (TextUtils.isEmpty(maxCost)) {
            maxCost = "0";
        }
        if (TextUtils.isEmpty(adults)) {
            adults = "0";
        }
        if (TextUtils.isEmpty(children)) {
            children = "0";
        }

        // tạo chuyến đi
        if (cancle) {

        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            long start = 0, end = 0;

            try {
                start = dateFormat.parse(startDate).getTime();
                end = dateFormat.parse(endDate).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            final CreateTourRequest createTourRequest = new CreateTourRequest();
            createTourRequest.setName(tourName);
            createTourRequest.setStartDate(Long.toString(start));
            createTourRequest.setEndDate(Long.toString(end));
            createTourRequest.setAdults(Integer.parseInt(adults));
            createTourRequest.setChilds(Integer.parseInt(children));
            createTourRequest.setMinCost(Integer.parseInt(minCost));
            createTourRequest.setMaxCost(Integer.parseInt(maxCost));
            createTourRequest.setIsPrivate(isPrivate);
            createTourRequest.setSourceLat(latLngStart.latitude);
            createTourRequest.setSourceLong(latLngStart.longitude);
            createTourRequest.setDesLat(latLngEnd.latitude);
            createTourRequest.setDesLong(latLngEnd.longitude);

            Call<CreateTourResponse> call = userService.createTour(MyAPIClient.getInstance().getAccessToken(), createTourRequest);
            call.enqueue(new Callback<CreateTourResponse>() {
                @Override
                public void onResponse(Call<CreateTourResponse> call, Response<CreateTourResponse> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), getString(R.string.create_tour_success), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), StopActivity.class);
                        intent.putExtra("TOUR_ID",response.body().getId());
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.create_tour_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CreateTourResponse> call, Throwable t) {
                    Log.d("CREATE_TOUR_ACTIVITY", t.getMessage());

                }
            });
        }
    }

    private void AnhXa() {
        btnCreateTour = findViewById(R.id.btnCreateTour);
        edtTourName = findViewById(R.id.edtTourName);
        edtStartDate = findViewById(R.id.edtStartDate);
        edtEndDate = findViewById(R.id.edtEndDate);
        edtAdults = findViewById(R.id.edtAdults);
        edtChildren = findViewById(R.id.edtChildren);
        edtMinCost = findViewById(R.id.edtMinCost);
        edtMaxCost = findViewById(R.id.edtMaxCost);
        cbIsPrivate = findViewById(R.id.cbIsPrivate);
        btnStartPoint=findViewById(R.id.btnStartPoint);
        btnEndPoint=findViewById(R.id.btnEndPoint);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_MAP_START_POINT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                latLngStart = (LatLng) data.getParcelableExtra("picked_point");
                Toast.makeText(this, "Điểm đã chọn: " + latLngStart.latitude + " " + latLngStart.longitude, Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == PICK_MAP_END_POINT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                latLngEnd = (LatLng) data.getParcelableExtra("picked_point");
                Toast.makeText(this, "Điểm đã chọn: " + latLngEnd.latitude + " " + latLngEnd.longitude, Toast.LENGTH_LONG).show();
            }
        }
    }
}
