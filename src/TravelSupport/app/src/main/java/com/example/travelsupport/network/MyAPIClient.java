package com.example.travelsupport.network;

import android.text.TextUtils;

import com.example.travelsupport.manager.Constants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyAPIClient {
    private Retrofit retrofit;
    private static MyAPIClient instance;
    private String accessToken;
    private String userID;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    private MyAPIClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.APIEndpoint)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static MyAPIClient getInstance() {
        if (instance == null)
            instance = new MyAPIClient();
        return instance;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
