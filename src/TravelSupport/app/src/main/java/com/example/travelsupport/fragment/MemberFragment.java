package com.example.travelsupport.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelsupport.R;
import com.example.travelsupport.adapter.MemberAdapter;
import com.example.travelsupport.model.GetTourInfoResponse;
import com.example.travelsupport.model.Member;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberFragment extends Fragment {
    private UserService userService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_member, container, false);

        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);


        Integer tourId = getActivity().getIntent().getExtras().getInt("tourId");

        Call<GetTourInfoResponse> call = userService.getTourInfo(MyAPIClient.getInstance().getAccessToken(), tourId);

        call.enqueue(new Callback<GetTourInfoResponse>() {
            @Override
            public void onResponse(Call<GetTourInfoResponse> call, Response<GetTourInfoResponse> response) {
                if (response.isSuccessful()) {
                    List<Member> memberList = response.body().getMembers();
                    ArrayList<Member> memberArrayList = new ArrayList<Member>(memberList);

                    ListView lvMember = view.findViewById(R.id.lvMember);
                    MemberAdapter memberAdapter = new MemberAdapter(getActivity(), R.layout.member_item, memberArrayList);

                    lvMember.setAdapter(memberAdapter);

                }
            }

            @Override
            public void onFailure(Call<GetTourInfoResponse> call, Throwable t) {

            }
        });

        return view;
    }
}
