package com.example.travelsupport.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.travelsupport.R;
import com.example.travelsupport.model.StopPoint2;
import com.example.travelsupport.model.StopPoint3;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class StopPointAdapter2 extends ArrayAdapter<StopPoint3> {
    Context context;
    int resource;
    private List<StopPoint3> arrStopPoint;

    public StopPointAdapter2(@NonNull Context context, int resource, ArrayList<StopPoint3> arrStopPoint) {
        super(context, resource, arrStopPoint);
        this.context = context;
        this.resource = resource;
        this.arrStopPoint = arrStopPoint;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.destination_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgAvatar = convertView.findViewById(R.id.imgAvatar);
            viewHolder.txtName = convertView.findViewById(R.id.txtName);
            viewHolder.txtLocation = convertView.findViewById(R.id.txtLocation);
            viewHolder.txtPrice = convertView.findViewById(R.id.txtPrice);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        StopPoint3 stopPoint = arrStopPoint.get(position);

        if (stopPoint.getAddress() == null) {
            viewHolder.txtLocation.setText("no address");
        } else {
            viewHolder.txtLocation.setText(stopPoint.getAddress().toString());
        }
        viewHolder.txtName.setText(stopPoint.getName());
        String price = stopPoint.getMinCost() + " - " + stopPoint.getMaxCost();
        viewHolder.txtPrice.setText(price + " VND");


        return convertView;
    }

    private class ViewHolder {
        TextView txtName, txtLocation, txtPrice;
        ImageView imgAvatar;
    }
}
