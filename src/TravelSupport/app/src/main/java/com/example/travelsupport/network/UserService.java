package com.example.travelsupport.network;

import com.example.travelsupport.model.AddServiceFeedbackRequest;
import com.example.travelsupport.model.AddServiceFeedbackResponse;
import com.example.travelsupport.model.AddStopPointRequest;
import com.example.travelsupport.model.AddStopPointResponse;
import com.example.travelsupport.model.AddTourReviewRequest;
import com.example.travelsupport.model.AddTourReviewResponse;
import com.example.travelsupport.model.CreateTourRequest;
import com.example.travelsupport.model.CreateTourResponse;
import com.example.travelsupport.model.ForgetPasswordRequest;
import com.example.travelsupport.model.ForgetPasswordResponse;
import com.example.travelsupport.model.GetServiceFeedbackResponse;
import com.example.travelsupport.model.GetSuggestDestinationRequest;
import com.example.travelsupport.model.GetSuggestDestinationResponse;
import com.example.travelsupport.model.GetTourInfoResponse;
import com.example.travelsupport.model.GetTourReviewResponse;
import com.example.travelsupport.model.GetUserInfoResponse;
import com.example.travelsupport.model.HistoryTourResponse;
import com.example.travelsupport.model.LoginByFacebookRequest;
import com.example.travelsupport.model.LoginByFacebookResponse;
import com.example.travelsupport.model.LoginRequest;
import com.example.travelsupport.model.LoginResponse;
import com.example.travelsupport.model.SearchDestinationResponse;
import com.example.travelsupport.model.SearchHistoryResponse;
import com.example.travelsupport.model.SignupRequest;
import com.example.travelsupport.model.SignupResponse;
import com.example.travelsupport.model.TourListResponse;
import com.example.travelsupport.model.UpdateUserInfoRequest;
import com.example.travelsupport.model.UpdateUserInfoResponse;
import com.example.travelsupport.model.VerifyOTPRequest;
import com.example.travelsupport.model.VerifyOTPResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserService {
    @POST("/user/login")
    Call<LoginResponse> login(@Body LoginRequest request);

    @POST("/user/login/by-facebook")
    Call<LoginByFacebookResponse> loginByFacebook(@Body LoginByFacebookRequest request);

    @POST("/user/register")
    Call<SignupResponse> signup(@Body SignupRequest request);

    @GET("/tour/list")
    Call<TourListResponse> getListTour(@Header("Authorization") String token, @Query("rowPerPage") Integer rowPerPage, @Query("pageNum") Integer pageNum);

    @POST("/tour/create")
    Call<CreateTourResponse> createTour(@Header("Authorization") String token, @Body CreateTourRequest request);

    @GET("/tour/history-user")
    Call<HistoryTourResponse> historyTour(@Header("Authorization") String token, @Query("pageIndex") Integer pageIndex, @Query("pageSize") Integer pageSize);

    @POST("tour/set-stop-points")
    Call<AddStopPointResponse> addStopPoint(@Header("Authorization") String token, @Body AddStopPointRequest request);

    @GET("/user/info")
    Call<GetUserInfoResponse> getUserInfo(@Header("Authorization") String token);

    @POST("/user/edit-info")
    Call<UpdateUserInfoResponse> updateInfo(@Header("Authorization") String token, @Body UpdateUserInfoRequest request);

    @GET("/tour/search/service")
    Call<SearchDestinationResponse> searchDestination(@Header("Authorization") String token, @Query("searchKey") String searchKey, @Query("pageIndex") Integer pageIndex, @Query("pageSize") Integer pageSize);

    @POST("/user/request-otp-recovery")
    Call<ForgetPasswordResponse> forgetPassword(@Body ForgetPasswordRequest request);

    @POST("/user/verify-otp-recovery")
    Call<VerifyOTPResponse> verifyOTP(@Body VerifyOTPRequest request);

    @GET("/tour/search-history-user")
    Call<SearchHistoryResponse> searchHistory(@Header("Authorization") String token, @Query("searchKey") String searchKey, @Query("pageIndex") Integer pageIndex, @Query("pageSize") Integer pageSize);

    @GET("/tour/info")
    Call<GetTourInfoResponse> getTourInfo(@Header("Authorization") String token, @Query("tourId") Integer tourId);

    @GET("/tour/get/review-list")
    Call<GetTourReviewResponse> getTourReview(@Header("Authorization") String token, @Query("tourId") Integer tourId, @Query("pageIndex") Integer pageIndex, @Query("pageSize") Integer pageSize);

    @GET("/tour/get/feedback-service")
    Call<GetServiceFeedbackResponse> getServiceFeedback(@Header("Authorization") String token, @Query("serviceId") Integer serviceId, @Query("pageIndex") Integer pageIndex, @Query("pageSize") Integer pageSize);

    @POST("/tour/add/review")
    Call<AddTourReviewResponse> addTourReview(@Header("Authorization") String token, @Body AddTourReviewRequest request);

    @POST("/tour/suggested-destination-list")
    Call<GetSuggestDestinationResponse> getSuggestDestination(@Header("Authorization") String token, @Body GetSuggestDestinationRequest request);

    @POST("/tour/add/feedback-service")
    Call<AddServiceFeedbackResponse> addServiceFeedback(@Header("Authorization") String token, @Body AddServiceFeedbackRequest request);
}
