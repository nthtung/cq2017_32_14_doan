package com.example.travelsupport.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.travelsupport.R;
import com.example.travelsupport.model.Comment;
import com.example.travelsupport.model.Comment2;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends ArrayAdapter<Comment> {
    private Context context;
    private int resource;
    private List<Comment> commentList;

    public CommentAdapter(@NonNull Context context, int resource, ArrayList<Comment> commentList) {
        super(context, resource, commentList);
        this.context = context;
        this.resource = resource;
        this.commentList = commentList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.comment_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgUserAvatar = convertView.findViewById(R.id.imgUserAvatar);
            viewHolder.txtUserName = convertView.findViewById(R.id.txtUserName);
            viewHolder.txtUserComment = convertView.findViewById(R.id.txtUserComment);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Comment comment = commentList.get(position);

        if (comment.getAvatar() == null) {
            viewHolder.imgUserAvatar.setImageResource(R.drawable.ic_user);
        } else {
            Picasso.get().load(comment.getAvatar().toString()).into(viewHolder.imgUserAvatar);
        }

        String username = comment.getName();
        if (!TextUtils.isEmpty(username)) {
            viewHolder.txtUserName.setText(comment.getName());
        } else {
            viewHolder.txtUserName.setText("no username");
        }

        viewHolder.txtUserComment.setText(comment.getComment());

        return convertView;
    }

    private class ViewHolder {
        TextView txtUserName, txtUserComment;
        ImageView imgUserAvatar;
    }
}
