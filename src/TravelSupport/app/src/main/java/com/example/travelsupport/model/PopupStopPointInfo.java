package com.example.travelsupport.model;

import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.travelsupport.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class PopupStopPointInfo {
    private EditText edtName,edtAddress,edtArrive,edtLeave,edtMinCost,edtMaxCost;
    private int serviceType, provinceId;


    public void showPopupWindow(final View view, final List<StopPoint> stopPointList, final LatLng latLng) {
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_stop_point_info,null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView,width,height,focusable);

        view.findViewById(R.id.stopPointView).post(new Runnable() {
            @Override
            public void run() {
                popupWindow.showAtLocation(view, Gravity.CENTER,0,0);
            }
        });


        // Cài đặt cho layout
        // ánh xạ
        edtName=popupView.findViewById(R.id.edtStopPointName);
        edtAddress=popupView.findViewById(R.id.edtStopPointAddress);
        edtArrive=popupView.findViewById(R.id.edtStopPointArrive);
        edtLeave=popupView.findViewById(R.id.edtStopPointLeave);
        edtMinCost=popupView.findViewById(R.id.edtStopPointMinCost);
        edtMaxCost=popupView.findViewById(R.id.edtStopPointMaxCost);

        // cài đặt cho địa chỉ
        String address;
        Geocoder geocoder;
        List<Address>  addresses;
        geocoder=new Geocoder(view.getContext(),Locale.getDefault());
        try {
            addresses =geocoder.getFromLocation(latLng.latitude,latLng.longitude,1);
            address = addresses.get(0).getAddressLine(0);

        } catch (IOException e) {
            e.printStackTrace();
            address="";
        }
        edtAddress.setText(address);

        // cài đặt cho loại dịch vụ
        Spinner spinnerServiceType = popupView.findViewById(R.id.spinnerServiceType);
        ArrayAdapter<CharSequence> serviceTypeAdapter = ArrayAdapter.createFromResource(popupView.getContext(),R.array.service_type_array,R.layout.support_simple_spinner_dropdown_item);
        spinnerServiceType.setAdapter(serviceTypeAdapter);
        spinnerServiceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceType = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // cài đặt cho mã tỉnh
        Spinner spinnerProvinceID = popupView.findViewById(R.id.spinnerProvinceID);
        ArrayAdapter<CharSequence> provinceIDAdapter = ArrayAdapter.createFromResource(popupView.getContext(),R.array.province_id,R.layout.support_simple_spinner_dropdown_item);
        spinnerProvinceID.setAdapter(provinceIDAdapter);
        spinnerProvinceID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceId = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Xác nhận
        Button btnAddStopPoint = popupView.findViewById(R.id.btnAddStopPoint);
        // click xác nhận thì đóng cửa sổ popup
        btnAddStopPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean cancel = false;

                // lấy dữ liệu
                String name = edtName.getText().toString(),
                        address = edtAddress.getText().toString(),
                        arrive=edtArrive.getText().toString(),
                        leave=edtLeave.getText().toString(),
                        minCost=edtMinCost.getText().toString(),
                        maxCost=edtMaxCost.getText().toString();

                // kiểm tra
                if (TextUtils.isEmpty(name)) {
                    cancel=true;
                }
                if (TextUtils.isEmpty(address)) {
                    cancel=true;
                }
                if (TextUtils.isEmpty(arrive)) {
                    cancel=true;
                }
                if (TextUtils.isEmpty(leave)) {
                    cancel=true;
                }

                if (cancel) {

                } else {
                    // tạo và thêm vào danh sách điểm dừng
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    long arriveTime = 0, leaveTime = 0;

                    try {
                        arriveTime = dateFormat.parse(arrive).getTime();
                        leaveTime = dateFormat.parse(leave).getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }



                    StopPoint sp = new StopPoint();
                    sp.setName(name);
                    sp.setAddress(address);
                    sp.setProvinceId(provinceId);
                    sp.setLat(latLng.latitude);
                    sp.setLong(latLng.longitude);
                    sp.setArrivalAt(arriveTime);
                    sp.setLeaveAt(leaveTime);
                    sp.setServiceTypeId(serviceType);

                    stopPointList.add(sp);
                    Toast.makeText(view.getContext(), "Thành công", Toast.LENGTH_LONG).show();
                    popupWindow.dismiss();
                }
            }
        });

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // đóng cửa sổ popup
                popupWindow.dismiss();
                return true;
            }
        });
    }



}
