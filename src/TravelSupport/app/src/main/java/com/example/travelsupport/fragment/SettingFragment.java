package com.example.travelsupport.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelsupport.R;
import com.example.travelsupport.model.GetUserInfoResponse;
import com.example.travelsupport.model.UpdateUserInfoRequest;
import com.example.travelsupport.model.UpdateUserInfoResponse;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;
import com.example.travelsupport.view.LoginActivity;
import com.example.travelsupport.view.MainActivity;
import com.squareup.picasso.Picasso;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class SettingFragment extends Fragment {
    private UserService userService;
    private EditText edtUserName, edtUserDob, edtUserAddress, edtUserEmail, edtUserPhone;
    private Button btnUpdateInfo, btnLogout;
    private ProgressBar progressBar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_setting,container,false);
        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);

        RelativeLayout layout = this.getActivity().findViewById(R.id.mainLayout);
        progressBar = new ProgressBar(this.getActivity(),null,android.R.attr.progressBarStyle);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        layout.addView(progressBar, params);

        progressBar.setVisibility(View.GONE);


        // anh xa
        edtUserName=view.findViewById(R.id.edtUserName);
        edtUserDob=view.findViewById(R.id.edtUserDob);
        edtUserAddress=view.findViewById(R.id.edtUserAddress);
        edtUserEmail=view.findViewById(R.id.edtUserEmail);
        edtUserPhone=view.findViewById(R.id.edtUserPhone);
        btnUpdateInfo=view.findViewById(R.id.btnUpdateInfo);
        btnLogout=view.findViewById(R.id.btnLogout);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyAPIClient.getInstance().setAccessToken(null);
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.package_name), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove(getString(R.string.saved_access_token));
                editor.remove(getString(R.string.user_ID));
                editor.commit();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });


        btnUpdateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                UpdateUserInfoRequest request = new UpdateUserInfoRequest();
                String name = edtUserName.getText().toString(),
                        dob = edtUserDob.getText().toString();

                request.setFullName(name);
                request.setDob(dob);

                Call<UpdateUserInfoResponse> call = userService.updateInfo(MyAPIClient.getInstance().getAccessToken(),request);

                call.enqueue(new Callback<UpdateUserInfoResponse>() {
                    @Override
                    public void onResponse(Call<UpdateUserInfoResponse> call, Response<UpdateUserInfoResponse> response) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.success_update_info), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<UpdateUserInfoResponse> call, Throwable t) {

                    }
                });
            }
        });

        Call<GetUserInfoResponse> getUserInfo = userService.getUserInfo(MyAPIClient.getInstance().getAccessToken());
        getUserInfo.enqueue(new Callback<GetUserInfoResponse>() {
            @Override
            public void onResponse(Call<GetUserInfoResponse> call, Response<GetUserInfoResponse> response) {
                if (response.isSuccessful()) {
                    String avatarURL = response.body().getAvatar();
                    if (avatarURL != null) {
                        ImageView avatar = view.findViewById(R.id.userAvatar);
                        Picasso.get().load(avatarURL).into(avatar);
                    }

                    edtUserName.setText(response.body().getFullName());
                    edtUserEmail.setText(response.body().getEmail());
                    edtUserPhone.setText(response.body().getPhone());
                    if (response.body().getAddress() == null) {
                        edtUserAddress.setText("");
                    }
                    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    String dobString;
                    LocalDate dob;
                    try {
                        dob = org.joda.time.LocalDate.parse(response.body().getDob(),formatter);
                        dobString=dob.toString("yyyy-MM-dd");
                    } catch (Exception e) {
                        e.printStackTrace();
                        dobString = "";
                    }
                    edtUserDob.setText(dobString);
                }
            }

            @Override
            public void onFailure(Call<GetUserInfoResponse> call, Throwable t) {

            }
        });

        return view;
    }
}
