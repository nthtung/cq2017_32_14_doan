package com.example.travelsupport.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetServiceFeedbackResponse {
    @SerializedName("feedbackList")
    @Expose
    private List<Feedback> feedbackList = null;

    public List<Feedback> getFeedbackList() {
        return feedbackList;
    }

    public void setFeedbackList(List<Feedback> feedbackList) {
        this.feedbackList = feedbackList;
    }
}
