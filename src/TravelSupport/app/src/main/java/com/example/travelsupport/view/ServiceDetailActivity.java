package com.example.travelsupport.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.travelsupport.R;
import com.example.travelsupport.adapter.FeedbackAdapter;
import com.example.travelsupport.model.AddServiceFeedbackRequest;
import com.example.travelsupport.model.AddServiceFeedbackResponse;
import com.example.travelsupport.model.Feedback;
import com.example.travelsupport.model.GetServiceFeedbackResponse;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceDetailActivity extends AppCompatActivity {
    private UserService userService;
    private RatingBar ratingBar;
    private EditText edtFeedbackArea;
    private Button btnSendFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);

        final Integer serviceId = getIntent().getExtras().getInt("serviceId");

        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);

        ratingBar = findViewById(R.id.ratingBar);
        edtFeedbackArea = findViewById(R.id.edtFeedbackArea);
        btnSendFeedback = findViewById(R.id.btnSendFeedback);


        btnSendFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String feedback = edtFeedbackArea.getText().toString();
                Number point = ratingBar.getRating();


                AddServiceFeedbackRequest request = new AddServiceFeedbackRequest();

                request.setPoint(point);
                request.setFeedback(feedback);
                request.setServiceId(serviceId);

                Call<AddServiceFeedbackResponse> call = userService.addServiceFeedback(MyAPIClient.getInstance().getAccessToken(), request);

                call.enqueue(new Callback<AddServiceFeedbackResponse>() {
                    @Override
                    public void onResponse(Call<AddServiceFeedbackResponse> call, Response<AddServiceFeedbackResponse> response) {
                        if (response.isSuccessful()) {
                            Log.d("cba", "onResponse: ");
                            Toast.makeText(ServiceDetailActivity.this, getString(R.string.success), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<AddServiceFeedbackResponse> call, Throwable t) {
                    }
                });

            }
        });

        Call<GetServiceFeedbackResponse> call = userService.getServiceFeedback(MyAPIClient.getInstance().getAccessToken(), serviceId, 1, 10000);

        call.enqueue(new Callback<GetServiceFeedbackResponse>() {
            @Override
            public void onResponse(Call<GetServiceFeedbackResponse> call, Response<GetServiceFeedbackResponse> response) {
                if (response.isSuccessful()) {
                    List<Feedback> feedbackList = response.body().getFeedbackList();
                    ArrayList<Feedback> feedbackArrayList = new ArrayList<>(feedbackList);

                    ListView lvFeedback = findViewById(R.id.lvFeedback);
                    FeedbackAdapter feedbackAdapter = new FeedbackAdapter(getBaseContext(), R.layout.feedback_item, feedbackArrayList);
                    lvFeedback.setAdapter(feedbackAdapter);
                }
            }

            @Override
            public void onFailure(Call<GetServiceFeedbackResponse> call, Throwable t) {
                Log.d("SERVICE_FEEDBACK", t.getMessage());
            }
        });
    }
}
