package com.example.travelsupport.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.travelsupport.R;
import com.example.travelsupport.fragment.GeneralFragment;
import com.example.travelsupport.fragment.MemberFragment;
import com.example.travelsupport.fragment.ReviewFragment;
import com.example.travelsupport.fragment.StopPointFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class TourDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_detail);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_tour_detail);


        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_tour_detail, new GeneralFragment()).commit();

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;

            switch (menuItem.getItemId()) {
                case R.id.nav_general:
                    selectedFragment = new GeneralFragment();
                    break;
                case R.id.nav_stop_point:
                    selectedFragment = new StopPointFragment();
                    break;
                case R.id.nav_member:
                    selectedFragment = new MemberFragment();
                    break;
                case R.id.nav_review:
                    selectedFragment = new ReviewFragment();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_tour_detail, selectedFragment).commit();
            return true;
        }
    };
}
