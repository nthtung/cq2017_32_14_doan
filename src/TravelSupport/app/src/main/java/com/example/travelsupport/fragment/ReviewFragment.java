package com.example.travelsupport.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelsupport.R;
import com.example.travelsupport.adapter.ReviewAdapter;
import com.example.travelsupport.model.AddTourReviewRequest;
import com.example.travelsupport.model.AddTourReviewResponse;
import com.example.travelsupport.model.GetTourReviewResponse;
import com.example.travelsupport.model.Review;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewFragment extends Fragment {
    private UserService userService;
    private RatingBar ratingBar;
    private EditText edtReviewArea;
    private Button btnSendReview;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_review, container, false);

        final Integer tourId = getActivity().getIntent().getExtras().getInt("tourId");
        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);

        ratingBar=view.findViewById(R.id.ratingBar);
        edtReviewArea=view.findViewById(R.id.edtReviewArea);
        btnSendReview=view.findViewById(R.id.btnSendReview);



        btnSendReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userReview = edtReviewArea.getText().toString();
                final float point = ratingBar.getRating();
                AddTourReviewRequest request = new AddTourReviewRequest();
                request.setPoint(point);
                request.setReview(userReview);
                request.setTourId(tourId);

                Log.d("cba", "onClick: " + request.getReview());
                Log.d("cba", "onClick: " + request.getPoint());
                Log.d("cba", "onClick: " + request.getTourId());


                Call<AddTourReviewResponse> call = userService.addTourReview(MyAPIClient.getInstance().getAccessToken(),request);

                call.enqueue(new Callback<AddTourReviewResponse>() {
                    @Override
                    public void onResponse(Call<AddTourReviewResponse> call, Response<AddTourReviewResponse> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(getContext(), getString(R.string.success), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddTourReviewResponse> call, Throwable t) {

                    }
                });
            }
        });

        Call<GetTourReviewResponse> getReviewCall = userService.getTourReview(MyAPIClient.getInstance().getAccessToken(), tourId, 1, 10000);

        getReviewCall.enqueue(new Callback<GetTourReviewResponse>() {
            @Override
            public void onResponse(Call<GetTourReviewResponse> call, Response<GetTourReviewResponse> response) {
                if (response.isSuccessful()) {
                    List<Review> reviewList = response.body().getReviewList();
                    final ArrayList<Review> reviewArrayList = new ArrayList<Review>(reviewList);
                    ListView lvReview = view.findViewById(R.id.lvReview);
                    ReviewAdapter reviewAdapter = new ReviewAdapter(getActivity(), R.layout.review_item, reviewArrayList);

                    lvReview.setAdapter(reviewAdapter);
                }
            }

            @Override
            public void onFailure(Call<GetTourReviewResponse> call, Throwable t) {

            }
        });



        return view;
    }
}
