package com.example.travelsupport.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddServiceFeedbackRequest {
    @SerializedName("serviceId")
    @Expose
    private Integer serviceId;
    @SerializedName("feedback")
    @Expose
    private String feedback;
    @SerializedName("point")
    @Expose
    private Number point;

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Number getPoint() {
        return point;
    }

    public void setPoint(Number point) {
        this.point = point;
    }
}
