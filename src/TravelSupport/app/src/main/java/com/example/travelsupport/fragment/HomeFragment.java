package com.example.travelsupport.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelsupport.R;
import com.example.travelsupport.adapter.TourAdapter;
import com.example.travelsupport.model.Tour;
import com.example.travelsupport.model.TourListRequest;
import com.example.travelsupport.model.TourListResponse;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;
import com.example.travelsupport.view.CreateTourActivity;
import com.example.travelsupport.view.TourDetailActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    private UserService userService;
    private FloatingActionButton btnCreateTour;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home,container,false);
        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);
        btnCreateTour = view.findViewById(R.id.btnCreateTour);
        btnCreateTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTour();
            }
        });

        final TourListRequest request = new TourListRequest();
        request.setRowPerPage(10000);
        request.setPageNum(1);

        Call<TourListResponse> call = userService.getListTour(MyAPIClient.getInstance().getAccessToken(), request.getRowPerPage(), request.getPageNum());
        call.enqueue(new Callback<TourListResponse>() {
            @Override
            public void onResponse(Call<TourListResponse> call, Response<TourListResponse> response) {
                // hien thi tour dung listview
                List<Tour> listTour = response.body().getTours();
                final ArrayList<Tour> arrTour = new ArrayList<Tour>(listTour);
                ListView lvTour = view.findViewById(R.id.lvTour);

                lvTour.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Tour t = arrTour.get(position);

                        Intent intent = new Intent(getActivity(), TourDetailActivity.class);
                        intent.putExtra("tourId", t.getId());
                        startActivity(intent);
                    }
                });

                TourAdapter tourAdapter = new TourAdapter(getActivity(), R.layout.tour_item, arrTour);
                lvTour.setAdapter(tourAdapter);
            }

            @Override
            public void onFailure(Call<TourListResponse> call, Throwable t) {

            }
        });

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void createTour() {
        Intent intent = new Intent(getActivity(), CreateTourActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
