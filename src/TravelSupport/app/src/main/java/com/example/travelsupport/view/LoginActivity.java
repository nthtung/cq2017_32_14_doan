package com.example.travelsupport.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.travelsupport.R;
import com.example.travelsupport.model.ForgetPasswordRequest;
import com.example.travelsupport.model.ForgetPasswordResponse;
import com.example.travelsupport.model.LoginByFacebookRequest;
import com.example.travelsupport.model.LoginByFacebookResponse;
import com.example.travelsupport.model.LoginRequest;
import com.example.travelsupport.model.LoginResponse;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText edtEmailPhone, edtPassword;
    private Button btnSignIn;
    private LoginButton btnLoginFacebook;
    private UserService userService;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Ánh xạ
        edtEmailPhone = findViewById(R.id.edtEmailPhone);
        edtPassword = findViewById(R.id.edtPassword);
        btnSignIn = findViewById(R.id.bntSignIn);

        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);

        btnLoginFacebook = findViewById(R.id.login_button);

        callbackManager = CallbackManager.Factory.create();


        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });

        btnLoginFacebook.setPermissions("email");
        btnLoginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                LoginByFacebookRequest request = new LoginByFacebookRequest();
                request.setAccessToken(loginResult.getAccessToken().getToken());

                Call<LoginByFacebookResponse> call = userService.loginByFacebook(request);

                call.enqueue(new Callback<LoginByFacebookResponse>() {
                    @Override
                    public void onResponse(Call<LoginByFacebookResponse> call, Response<LoginByFacebookResponse> response) {
                        // lưu access token
                        MyAPIClient.getInstance().setAccessToken(response.body().getToken());
                        MyAPIClient.getInstance().setUserID(response.body().getUserId());
                        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.package_name), MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(getString(R.string.saved_access_token), response.body().getToken());
                        editor.putString(getString(R.string.user_ID), response.body().getUserId());
                        editor.commit();

                        // hiển thị màn hình danh sách tour
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }

                    @Override
                    public void onFailure(Call<LoginByFacebookResponse> call, Throwable t) {
                        Log.d("LOGIN_ACTIVITY", t.getMessage());
                    }
                });
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void LaunchSignup(View view) {
        Intent intent = new Intent(this, SignupActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void LaunchForgetPassword(View view) {
        String email = edtEmailPhone.getText().toString();
        ForgetPasswordRequest request = new ForgetPasswordRequest();
        request.setType("email");
        request.setValue(email);
        Call<ForgetPasswordResponse> call = userService.forgetPassword(request);

        call.enqueue(new Callback<ForgetPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                if (response.isSuccessful()) {
                    Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                    intent.putExtra("USER_ID", response.body().getUserId());
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
                Log.d("LOGIN_ACTIVITY", t.getMessage());
            }
        });
    }

    private void Login() {
        boolean cancel = false;

        // clear error
        edtEmailPhone.setError(null);
        edtPassword.setError(null);

        // Lấy tên đăng nhập và mật khẩu
        String emailPhone = edtEmailPhone.getText().toString();
        String password = edtPassword.getText().toString();

        // kiểm tra hợp lệ
        if (TextUtils.isEmpty(emailPhone)) {
            edtEmailPhone.setError(getString(R.string.empty_field));
            cancel = true;
        }
        if (TextUtils.isEmpty(password)) {
            edtPassword.setError(getString(R.string.empty_field));
            cancel = true;
        }

        // đăng nhập
        if (cancel) {

        } else {
            final LoginRequest loginRequest = new LoginRequest();
            loginRequest.setEmailPhone(emailPhone);
            loginRequest.setPassword(password);

            Call<LoginResponse> call = userService.login(loginRequest);

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    // không có token thì không thể đăng nhập
                    if (response.body() == null) {
                        Toast.makeText(LoginActivity.this, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        // lưu access token
                        MyAPIClient.getInstance().setAccessToken(response.body().getToken());
                        MyAPIClient.getInstance().setUserID(response.body().getUserId());
                        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.package_name), MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(getString(R.string.saved_access_token), response.body().getToken());
                        editor.putString(getString(R.string.user_ID), response.body().getUserId());
                        editor.commit();

                        // hiển thị màn hình danh sách tour
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.d("LOGIN_ACTIVITY", t.getMessage());
                }
            });
        }

    }
}
