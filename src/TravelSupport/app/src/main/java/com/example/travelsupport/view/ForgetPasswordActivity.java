package com.example.travelsupport.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.travelsupport.R;
import com.example.travelsupport.model.VerifyOTPRequest;
import com.example.travelsupport.model.VerifyOTPResponse;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {

    private EditText edtPasswordFG, edtConfirmPasswordFG, edtOTP;
    private Button btnResetPassword;
    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        final Integer userId = getIntent().getIntExtra("USER_ID", 0);

        edtPasswordFG = findViewById(R.id.edtPasswordFG);
        edtConfirmPasswordFG = findViewById(R.id.edtConfirmPasswordFG);
        edtOTP = findViewById(R.id.edtOTP);

        btnResetPassword = findViewById(R.id.btnResetPassword);
        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean cancel = false;

                userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);


                String newPass = edtPasswordFG.getText().toString(),
                        confirmNewPass = edtConfirmPasswordFG.getText().toString(),
                        otp = edtOTP.getText().toString();

                if (!newPass.equals(confirmNewPass)) {
                    Toast.makeText(ForgetPasswordActivity.this, getString(R.string.error_confirm_pass), Toast.LENGTH_SHORT).show();
                    cancel = true;
                }

                if (cancel) {

                } else {
                    VerifyOTPRequest request = new VerifyOTPRequest();
                    request.setNewPassword(newPass);
                    request.setUserId(userId);
                    request.setVerifyCode(otp);

                    Log.d("USER_ID", userId.toString());

                    Call<VerifyOTPResponse> call = userService.verifyOTP(request);
                    call.enqueue(new Callback<VerifyOTPResponse>() {
                        @Override
                        public void onResponse(Call<VerifyOTPResponse> call, Response<VerifyOTPResponse> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(ForgetPasswordActivity.this, getString(R.string.success), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<VerifyOTPResponse> call, Throwable t) {
                            Log.d("LOGIN_ACTIVITY", t.getMessage());
                            Toast.makeText(ForgetPasswordActivity.this, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
}
