package com.example.travelsupport.model;

import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.example.travelsupport.R;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopupListStopPoint {
    private UserService userService;
    public void showPopupWindow(final View view, final List<StopPoint> stopPointList, final int tourID) {
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_list_stop_point,null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        boolean focusable = true;

        final PopupWindow popupWindow = new PopupWindow(popupView,width,height,focusable);

        popupWindow.showAtLocation(view, Gravity.CENTER,0,0);

        // Khởi tạo listview - mỗi item là tên của điểm dừng
        List<String> stopPointName = new ArrayList<>();
        for (StopPoint stopPoint:stopPointList) {
            stopPointName.add(stopPoint.getName());
        }
        ListView lvStopPoint = popupView.findViewById(R.id.lvStopPoint);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(popupView.getContext(),android.R.layout.simple_list_item_1,stopPointName);
        lvStopPoint.setAdapter(adapter);

        Button btnAddAllStopPoint = popupView.findViewById(R.id.btnAddAllStopPoint);
        btnAddAllStopPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddStopPointRequest request = new AddStopPointRequest();
                request.setTourId(tourID);
                request.setStopPoints(stopPointList);

                userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);

                Call<AddStopPointResponse> call = userService.addStopPoint(MyAPIClient.getInstance().getAccessToken(),request);
                call.enqueue(new Callback<AddStopPointResponse>() {
                    @Override
                    public void onResponse(Call<AddStopPointResponse> call, Response<AddStopPointResponse> response) {
                        Toast.makeText(view.getContext(), "Thêm điểm dừng thành công", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<AddStopPointResponse> call, Throwable t) {

                    }
                });
            }
        });
        // Click vào vùng ngoài popup để đóng cửa sổ
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

    }


}
