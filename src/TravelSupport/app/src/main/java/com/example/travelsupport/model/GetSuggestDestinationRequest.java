package com.example.travelsupport.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSuggestDestinationRequest {
    @SerializedName("hasOneCoordinate")
    @Expose
    private Boolean hasOneCoordinate;
    @SerializedName("coordList")
    @Expose
    private List<CoordList> coordList = null;

    public Boolean getHasOneCoordinate() {
        return hasOneCoordinate;
    }

    public void setHasOneCoordinate(Boolean hasOneCoordinate) {
        this.hasOneCoordinate = hasOneCoordinate;
    }

    public List<CoordList> getCoordList() {
        return coordList;
    }

    public void setCoordList(List<CoordList> coordList) {
        this.coordList = coordList;
    }

}
