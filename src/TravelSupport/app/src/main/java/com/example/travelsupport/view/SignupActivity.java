package com.example.travelsupport.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.travelsupport.R;
import com.example.travelsupport.model.SignupRequest;
import com.example.travelsupport.model.SignupResponse;
import com.example.travelsupport.network.MyAPIClient;
import com.example.travelsupport.network.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {
    private EditText edtName, edtEmail, edtPhone, edtPass, edtConfirmPass;
    private Button btnSignUp;
    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        AnhXa();

        userService = MyAPIClient.getInstance().getRetrofit().create(UserService.class);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XuLy();
            }
        });
    }

    private void XuLy() {
        boolean cancel = false;
        // clear error
        edtEmail.setError(null);
        edtName.setError(null);
        edtPhone.setError(null);
        edtPass.setError(null);
        edtConfirmPass.setError(null);

        // lấy dữ liệu
        String name = edtName.getText().toString(),
                email = edtEmail.getText().toString(),
                phone = edtPhone.getText().toString(),
                pass = edtPass.getText().toString(),
                pass1 = edtConfirmPass.getText().toString();

        // Kiểm tra dữ liệu
        if (TextUtils.isEmpty(name)) {
            edtEmail.setError(getString(R.string.empty_field));
            cancel = true;
        }
        if (TextUtils.isEmpty(email)) {
            edtEmail.setError(getString(R.string.empty_field));
            cancel = true;
        }
        if (TextUtils.isEmpty(phone)) {
            edtPhone.setError(getString(R.string.empty_field));
            cancel = true;
        }
        if (TextUtils.isEmpty(pass)) {
            edtPass.setError(getString(R.string.empty_field));
            cancel = true;
        }
        if (TextUtils.isEmpty(pass1)) {
            edtConfirmPass.setError(getString(R.string.empty_field));
            cancel = true;
        }
        if (!pass1.equals(pass)) {
            edtConfirmPass.setError(getString(R.string.error_confirm_pass));
            cancel = true;
        }

        // đăng ký
        if (cancel) {

        } else {
            final SignupRequest signupRequest = new SignupRequest();
            signupRequest.setFullName(name);
            signupRequest.setEmail(email);
            signupRequest.setPhone(phone);
            signupRequest.setPassword(pass);

            Call<SignupResponse> call = userService.signup(signupRequest);

            call.enqueue(new Callback<SignupResponse>() {
                @Override
                public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                    // đăng ký thất bại
                    if (response.code() != 200) {
                        Toast.makeText(getApplicationContext(), getString(R.string.failed_signup), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.success_signup), Toast.LENGTH_LONG).show();

                        SignupActivity.this.finish();
                    }
                }

                @Override
                public void onFailure(Call<SignupResponse> call, Throwable t) {
                    Log.d("SIGNUP_ACTIVITY", t.getMessage());
                }
            });
        }

    }

    private void AnhXa() {
        edtName = findViewById(R.id.edtFullNameSU);
        edtEmail = findViewById(R.id.edtEmailSU);
        edtPhone = findViewById(R.id.edtPhoneSU);
        edtPass = findViewById(R.id.edtPasswordSU);
        edtConfirmPass = findViewById(R.id.edtConfirmPasswordSU);
        btnSignUp = findViewById(R.id.btnSignUpSU);
    }

}
