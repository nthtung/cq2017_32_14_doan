package com.example.travelsupport.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.travelsupport.R;
import com.example.travelsupport.model.Member;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MemberAdapter extends ArrayAdapter<Member> {
    private Context context;
    private int resource;
    private List<Member> memberList;

    public MemberAdapter(@NonNull Context context, int resource, ArrayList<Member> memberList) {
        super(context, resource, memberList);
        this.context=context;
        this.resource=resource;
        this.memberList=memberList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.member_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtName = convertView.findViewById(R.id.txtUserName);
            viewHolder.txtPhone = convertView.findViewById(R.id.txtUserPhone);
            viewHolder.txtHost = convertView.findViewById(R.id.txtIsHost);
            viewHolder.imgAvatar = convertView.findViewById(R.id.imgUserAvatar);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Member member = memberList.get(position);

        if(member.getAvatar() == null) {
            viewHolder.imgAvatar.setImageResource(R.drawable.ic_user);
        } else {
            Picasso.get().load(member.getAvatar().toString()).into(viewHolder.imgAvatar);
        }

        String userName = member.getName();
        if (!TextUtils.isEmpty(userName)) {
            viewHolder.txtName.setText(userName);
        } else {
            viewHolder.txtName.setText("no username");
        }

        viewHolder.txtPhone.setText(member.getPhone());

        boolean isHost = member.getIsHost();

        if (isHost) {
            viewHolder.txtHost.setText("Chủ tour");
            viewHolder.txtHost.setTextColor(Color.parseColor("#D81B60"));
        } else {
            viewHolder.txtHost.setText("Thành viên");
        }

        return convertView;
    }

    private class ViewHolder {
        ImageView imgAvatar;
        TextView txtName, txtPhone, txtHost;
    }
}
